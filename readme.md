![](https://beta.designforventures.co/Free-Animated-SVG-Icons/warehouse/banners/RobotoBoldAnimated.jpg)
# Roboto Bold Animated — A Free Animated SVG Alphabet
Wouldn’t it be nice to have a pre-animated A-Z SVG alphabet in a nice font like Roboto? Of course. Free? Yes!

All characters come as separately animated .svgs so you can build your own sentences. Also included is each characters .keyshape file and the .sketch file for even further customization, like changing the timing of the animation or setting up your very own font. Awesome!

![](https://beta.designforventures.co/Free-Animated-SVG-Icons/warehouse/banners/AnimatedRobotoBanner.gif)

## Release notes v1.0
- A smoothly animated A–Z alphabet using Roboto Bold (Google Fonts)
- Ready to use pre-animated .SVGs
- Keyshape project files for each character so that you can build your own sentences (requires [Keyshape app](https://www.keyshapeapp.com/))
- Sketch template so you can use it to create your own alphabets or expand this one (requires [Sketch app](https://www.sketchapp.com/))
- Demo projects so you can learn more what Keyshape can do

## Related tutorials
- [Overview video of this very release](https://youtu.be/-cgeiDn-0Cs)
- [How to create the line effect used in this alphabet](https://youtu.be/_II48q6Q7KQ)
- [Two great Keyshape tricks that will enhance your animations](https://youtu.be/VmTP50sKCwQ)

## Download
[Boom-shaka-lak](https://bitbucket.org/designforventures/animated-roboto-bold-svg-a-z-alphabet/downloads/)